$(function(){
  $('[data-toggle="tooltip"]').tooltip();
  $('[data-toggle="popover"]').popover();
  $('.carousel').carousel({
    interval: 4000
  });
  //MODAL MOSTRANDO
  //Remove class al boton de contacto, desabilita, y cambia de clase
  $('#contactoModal').on('show.bs.modal', function (e){
    console.log('el modal contacto se esta mostrando');
    $('[data-target="#contactoModal"]').removeClass('btn-contacto');
    $('[data-target="#contactoModal"]').addClass('btn-contacto2');
    $('[data-target="#contactoModal"]').prop('disabled', true);
  });
  //MODAL MOSTRÓ
  $('#contactoModal').on('shown.bs.modal', function (e){
    console.log('el modal contacto se mostró');
  });
  //MODAL CERRANDO
  $('#contactoModal').on('hide.bs.modal', function (e){
    console.log('el modal contacto se oculta');
  });
  //MODAL CERRÓ
  //Remove class al boton de contacto, desabilita, y cambia de clase
  $('#contactoModal').on('hidden.bs.modal', function (e){
    console.log('el modal contacto se ocultó');
    $('[data-target="#contactoModal"]').removeClass('btn-contacto2');
    $('[data-target="#contactoModal"]').addClass('btn-contacto');
    $('[data-target="#contactoModal"]').prop('disabled', false);
  });
});